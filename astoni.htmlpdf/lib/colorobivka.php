<?

namespace Astoni\Htmlpdf;



class P

{

    public static function getDataClass($nameEntity = null)
    {
        \Bitrix\Main\Loader::IncludeModule("highloadblock");
        if ($nameEntity != null) {
            $result = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter' => array('=NAME' => $nameEntity)));
            if ($row = $result->fetch()) {
                $HLBLOCK_ID = $row["ID"];
            }
        } else {
            $result = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter' => array('=NAME' => 'ColorUpholstery')));
            if ($row = $result->fetch()) {
                $HLBLOCK_ID = $row["ID"];
            }
        }

        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($HLBLOCK_ID)->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);

        $entityDataClass = $entity->getDataClass();

        return $entityDataClass;
    }

    /**
     * @param array $filter ["UF_XML_ID"=>'#828282']
     */
    public static function getList($filter)
    {
       // print_r($filter);
        $entityDataClass = self::getDataClass(null);
        $result = $entityDataClass::getList(array(

            "select" => array("*"),
            "order" => array("UF_SORT" => "ASC"),
            "filter" => $filter,

        ));

        while ($arRow = $result->Fetch()) {

            $arReturn[$arRow['UF_XML_ID']] = $arRow;
        }
        return $arReturn;
    }
}
