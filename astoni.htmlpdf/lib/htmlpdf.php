<?

namespace Astoni\Htmlpdf;



class Htmlpdf

{
    function __construct() {
        $this->url_api = \COption::GetOptionString("astoni.htmlpdf", "url_api_server");
        $this->token_api_server = \COption::GetOptionString("astoni.htmlpdf", "token_api_server");
    }

    private function base64ToFile($base64string, $path)
    {


        $pdf_decoded = base64_decode($base64string);
        //Write data back to pdf file
        $pdf = fopen($path, 'w');
        fwrite($pdf, $pdf_decoded);
        //close output file
        fclose($pdf);
        //echo 'Done';
    }

    private function send_request ($body, $url_api) {
        $curl = curl_init($url_api);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($array, '', '&'));

        $out = curl_exec($curl);

        curl_close($curl);



        $out = json_decode($out);

        return $out;
    }


    public function getSrcDownload($filename) {
        $url_api = $this->url_api;
        $token_api_server = $this->token_api_server;

        if (!$url_api) return 'Не установлен url api сервера';
        if (!$token_api_server) return 'Не установлен token';

        $body = array(
            'link'    => $url,
            'token' => $token_api_server,
            'action' => 'getSrcDownload',
        );

        

        $this->out = $this->send_request($body,$url_api);

        return $this->out->urlDownload;
        
    }

    public function getFile($url) {
        
        $url_api = $this->url_api;
        $token_api_server = $this->token_api_server;

        if (!$url_api) return 'Не установлен url api сервера';
        if (!$token_api_server) return 'Не установлен token';

        $body = array(
            'link'    => $url,
            'token' => $token_api_server,
            'action' => 'getFile',
        );

        

        $this->out = $this->send_request($body,$url_api);

        
        return $this->out->status;

       
    }

    public function saveFileInBitrix($filename) {
        $pathFile= '/';

        $out = $this->out;
        if ($out->status == 'success') {

            $base64string = $out->base64string;

            $link = $pathFile.$filename.'.pdf';

            $path = $_SERVER['DOCUMENT_ROOT'] . $link;

            self::base64ToFile($base64string, $path);

            $fileArr = \CFile::MakeFileArray($path);
            $idFile = \CFile::SaveFile($fileArr, "bills", true);

            $src = \CFile::GetPath($idFile);


            @unlink($path);

            return [
                "id" => $idFile,
                "name" => $filename.'.pdf',
                'src' => $src
            ];
        }
    }


    public function saveFileInPath($filename,$pathFile= '/') {

        $out = $this->out;
        if ($out->status == 'success') {

            $base64string = $out->base64string;

            $link = $pathFile.$filename.'.pdf';

            $path = $_SERVER['DOCUMENT_ROOT'] . $link;

            self::base64ToFile($base64string, $path);

            //$fileArr = \CFile::MakeFileArray($path);
            //$idFile = \CFile::SaveFile($fileArr, "bills", true);

            //$src = \CFile::GetPath($idFile);

            return [
                'path'=>$path
            ];

        }
    }

}
