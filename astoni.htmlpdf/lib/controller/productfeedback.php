<?

namespace Astoni\Htmlpdf\Controller;

use Bitrix\Main\Engine\Controller;
use \Astoni\Htmlpdf\Basket;

class Productfeedback extends Controller
{
    protected function init()
    {
        parent::init();

        $this->setActionConfig('downloadfile', [
            '-prefilters' => [
                '\Bitrix\Main\Engine\ActionFilter\Authentication'
            ]
        ]);

        $this->setActionConfig('setitemquantity', [
            '-prefilters' => [
                '\Bitrix\Main\Engine\ActionFilter\Authentication'
            ]
        ]);
        $this->setActionConfig('deleteitem', [
            '-prefilters' => [
                '\Bitrix\Main\Engine\ActionFilter\Authentication'
            ]
        ]);

        $this->setActionConfig('clearbasket', [
            '-prefilters' => [
                '\Bitrix\Main\Engine\ActionFilter\Authentication'
            ]
        ]);
    }

    public function setitemquantityAction($param = '')
    {
        $productId = $param['ID'];
        $quantity = $param['QUANTITY'];

        Basket::setItemQuantity($productId, $quantity);


        return ['response' => 'success',  'data' => $param];
    }

    public function deleteitemAction($param = '')
    {
        $productId = $param['ID'];

        Basket::deleteItem($productId);


        return ['response' => 'success',  'data' => $param];
    }

    public function downloadfileAction($param = '')
    {
        $FILE = $param['FILES'];
        move_uploaded_file($FILE['file']['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . $FILE['file']['name']);
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . $FILE['file']['name'])) {
            $arFile = \CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . $FILE['file']['name']);
            $file = \CFile::SaveFile($arFile, 'vars');
        }


        return ['response' => 'success',  'file' => $file, 'param' => $param];
    }

    /**
     * if(\Astoni\Htmlpdf\Orders_message::add([
     *      'UF_ORDER_MESSAGES_CREATOR'=>$param['user_id'],
     *      'UF_ORDER_MESSAGES_ORDER_ID'=>$param['orderId'],
     *      'UF_ORDER_MESSAGES_RECIPIENT'=>$param['shipper_user_id'],
     *      'UF_ORDER_MESSAGES_TEXT'=>$param['comment']
     *  ])) {
     *     \Astoni\Htmlpdf\Order::setStatusOrder($param['orderId'], $param['status_id']);
     *  }
     */
    public function newmessageorderAction($param = '')
    {
        if (\Astoni\Htmlpdf\Orders_message::add([
            'UF_ORDER_MESSAGES_CREATOR' => $param['user_id'],
            'UF_ORDER_MESSAGES_ORDER_ID' => $param['orderId'],
            'UF_ORDER_MESSAGES_RECIPIENT' => $param['shipper_user_id'],
            'UF_ORDER_MESSAGES_TEXT' => $param['comment']
        ])) {
            \Astoni\Htmlpdf\Order::setStatusOrder($param['orderId'], $param['status_id']);
        }


        return ['response' => 'success', 'param' => $param];
    }


    public function newmessageAction($param = '')
    {

        if ($idChat = \Astoni\Htmlpdf\Chat::add(['UF_CHAT_TITLE' => $param['MESSAGE'], 'UF_CHAT_RECIPIENT' => $param['RESIPIENT']])->getId()) {
            \Astoni\Htmlpdf\Message::add(['UF_MESSAGES_RECIPIENT' => $param['RESIPIENT'], 'UF_MESSAGES_CHAT' => $idChat, 'UF_MESSAGES_MESSAGE' => $param['MESSAGE']]);
        }


        return ['response' => 'success', 'param' => $idChat];
    }


    public function addordernewdrouctAction($param = '')
    {
        $res = \Astoni\Htmlpdf\Order::create([$param['product_id'] => 1], [
            'email' => $param['email'],
            'user_id' => $param['user_id'],
            'shipper_id' => $param['shipper_id'],
            'shipper_user_id' => $param['shipper_user_id'],
            'file' => '',
            'comment' => $param['comment'],
            'request_subject' => $param['comment'],
        ]);
        if (!empty($res)) {
            //\CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
            $regMessage = (!\Bitrix\Main\Engine\CurrentUser::get()->getId()) ? '<br> На почтовый ящик ' . $param['email'] . ' отправлены регистрационные данные для авторизации.' : '';
            $message = '<div class="bg-white">Ваш заказ успешно оформлен. Для отслеживания статуса перейдите в <a href="/personal/">личный кабинет</a>' . $regMessage . '</div>';
        }

        return ['response' => 'success', 'message' => $message];
    }


    public function favproductAction($param = '')
    {
        switch ($param['action']) {
            case 'add':
                \AstHelper::addFavProduct($param['id']);
                break;
            case 'delete':
                \AstHelper::deleteFavProduct($param['id']);
                break;
            case 'list':
                
                return ['response' => 'success', 'favlist' => \AstHelper::getFavProducts()];
                break;
            default:
                # code...
                break;
        }
        return ['response' => 'success', 'id' => $param['id']];
    }

    public function clearbasketAction($param = '')
    {
		\CModule::IncludeModule("sale");
        $res = \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());
        return ['response' => 'success', 'res' => $res];
    }

}
