<?
$MESS['HTMLPDF_OPTIONS_TAB_NAME'] = 'Настройки';
$MESS['HTMLPDF_OPTIONS_TAB_COMMON'] = 'Настройки';
$MESS['HTMLPDF_OPTIONS_TAB_CHECKBOX'] = 'Пример поля чекбокс';
$MESS['HTMLPDF_OPTIONS_TAB_STRING'] = 'Пример поля типа строка';
$MESS['HTMLPDF_OPTIONS_TAB_SELECT'] = 'Пример поля селект';
$MESS['HTMLPDF_OPTIONS_TAB_MULTISELECT'] = 'Пример поля селект (множ)';
$MESS['HTMLPDF_OPTIONS_TAB_STATICTEXT'] = 'Пример статического текстового поля';
$MESS['HTMLPDF_OPTIONS_TAB_STATICHTML'] = 'Пример статического HTML поля';
$MESS['HTMLPDF_OPTIONS_TAB_TEXTAREA'] = 'Пример поля ввода текста';
$MESS['HTMLPDF_OPTIONS_TAB_MESSAGE'] = 'Пример вывода сообщения';
$MESS['HTMLPDF_OPTIONS_APPLY'] = 'Сохранить';
$MESS['HTMLPDF_EXPIRED'] = 'Закончился демо-режим модуля. Для продолжения работы вам нужно <a href="#" target="_blank">приобрести полнофункциональную версию (ссылка на маркетплейс)</a>.';
$MESS['HTMLPDF_OPTIONS_DEV'] = 'Разработчик модуля';
$MESS['HTMLPDF_OPTIONS_SUPPORT'] = '<a href="#" target="_blank">ссылка на сайт</a>';
$MESS['HTMLPDF_OPTIONS_SUPPORT_DATA'] = '<a href="#" target="_blank">ссылка на сайт</a>';


$MESS['HTMLPDF_OPTIONS_URL_API_SERVER'] = 'URL API Server';
$MESS['HTMLPDF_OPTIONS_TOKEN_API_SERVER'] = 'Ключ доступа';