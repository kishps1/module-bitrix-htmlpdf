<?
$MESS['HTMLPDF_NAME'] = 'Конвертор .html в .pdf';
$MESS['HTMLPDF_MODULE_DESCRIPTION'] = 'Модуль для создания .pdf файлов из .html';
$MESS['HTMLPDF_PARTNER_NAME'] = 'Astoni';
$MESS["HTMLPDF_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста, обновите систему.";
$MESS["HTMLPDF_PARTNER_URI"] = "https://www.astoni.ru/";