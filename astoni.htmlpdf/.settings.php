<?php

return [
    'controllers' => [
        'value' => [
            'defaultNamespace' => '\\Astoni\\Htmlpdf\\Controller',
            'namespaces' => [
                // Ключ - неймспейс для ajax-классов,
                // api - приставка экшенов, о ней мы поговорим чуть позже
                '\\Astoni\\Htmlpdf\\Controller' => 'apiHtmlpdf',
            ],
        ],
        'readonly' => true
    ],
];